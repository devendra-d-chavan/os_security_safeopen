LSM Module for Safe-Open
=========================
Project for CSC705 - OS Security by Dr. Enck

Goal
------------------------
A Linux Security Module for preventing link traversal attacks based on the 
*safe-open* concept from [Chari et al.][2]

Report compilation instructions
-----------------------
 `>xelatex --shell-escape answers.tex`

 Requires `xelatex` with `input-minted` package (and others)

Links
------------------------

 * [Project description page][1]
 * [Where Do You Want to Go Today? Escalating Privileges by Pathname 
 Manipulation][2]

 
 [1]: http://www.csc.ncsu.edu/faculty/enck/csc705-s14/projects.html]
 [2]: http://www.csc.ncsu.edu/faculty/enck/csc705-s14/docs/chari-etal.pdf
